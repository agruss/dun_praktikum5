import urllib2

def oracle (s) :
  url = "http://www.its.fh-muenster.de"
  port = 31337
  
  request = urllib2.Request(url + ":" + str(port))
  request.add_header('ciphertext', s);
  
  response = urllib2.urlopen(request)
  #debug
  #print response.getcode()
  #print response.read()
  if response.getcode() == 200:
    if response.read() in "decryption done":
      return 1
    else:
      return 0
  

#print oracle("11223344556677889900112233445566|1c2742994b32a41ecb1f20ab11b45899")