from aufg1 import oracle
import binascii

checkArray = "0123456789abcdef"

def crackme():
  block1 = "11223344556677889900112233445566"
  block2 = "1c2742994b32a41ecb1f20ab11b45899"
  #block2 = "fe9148489ee04c124202266cc457200e" #sebis
  #block2 = "564dfd86e96555d5fef4413ff001c07d"
  iv = "00000000000000000000000000000000"
  plaintext = "00000000000000000000000000000000"
  
  k = 30
  padding = 1
  while k >= 0:
    #pruefe Padding von 00 bis ff durch
    iv = checkPadding("00", k, iv, block2)
    
    #Klartext holen
    byteBlock1 = getByte(block1, k)
    byteIV = getByte(iv, k)
    
    bytePlaintext = hex(int(byteBlock1, 16) ^ int(byteIV, 16) ^ padding)
    if len(str(bytePlaintext)) == 3:
      newBytePlaintext = "0" + str(bytePlaintext[2])
    else:
      newBytePlaintext = str(bytePlaintext[2]+bytePlaintext[3])
    plaintext = string_replace(plaintext, k, newBytePlaintext)

    #naechsten Wert berechnen und in IV schreiben
    padding += 1
    
    j = k
    # alle IVs neu berechnen mti aktuellem Padding
    while j <= 30:
      #print "k:" +str(k) + "j:" + str(j)
      
      #print str(byteBlock1) + "^" + str(newBytePlaintext) + "^" + str(padding)
      newIV = hex(int(byteBlock1, 16) ^ int(newBytePlaintext, 16) ^ padding)
      if (len(str(newIV))) == 3:
	newByteIV = "0" + str(newIV[2])
      else:
	newByteIV = str(newIV[2]+newIV[3])
      iv = string_replace(iv, j, newByteIV)
      #print "neuer iv: " + iv
      
      j += 2
      try:
	byteBlock1 = getByte(block1, j)
	newBytePlaintext = getByte(plaintext, j)
      except IndexError:
	break

    print "IV: " + iv
    print "Klartext: " + plaintext
    k -= 2
  printPlaintext(plaintext)


def getByte(block, pos):
  "Gibt das entsprechende Byteblock als String zurueck"
  return block[pos]+block[pos+1]

def checkPadding( startByte, pos, iv, chiffrat ):
  "Prueft das Padding mittels der oracle Funktion"
  highByte = int(startByte[0], 16)
  lowByte = int(startByte[1], 16)

  for i in range(highByte,16):
    for j in range(lowByte,16):
      iv = string_replace(iv,pos,checkArray[i]+checkArray[j])
      if oracle(iv + "|" + chiffrat) == 1:
	#debug
	#print "erfolg@:" + checkArray[i]+checkArray[j]
	return iv
  return -1

def string_replace(oldString, index, ersatz):
  newString = list(oldString)
  newString[index] = ersatz[0]
  newString[index+1] = ersatz[1]
  oldString = ''.join(newString)

  return oldString
  
def printPlaintext(plaintext):
  "Gibt den Klartext aus"
  print binascii.unhexlify(plaintext)

crackme()